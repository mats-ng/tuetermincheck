# tuetermincheck

Dieses Programm überprüft exemplarisch, wie viele Tage man nun bei der (dauerhaft verpflichtend geplanten) Nutzung des städtischen Online-Portals auf einen Termin im Bürgeramt der Stadt Tübingen warten muss, z.B. für eine Ummeldung bei Wohnungswechsel - dies konnte zuvor mit einem spontanen Besuch des Bürgeramts und max. 30 Minuten Wartezeit erledigt werden...

  * [Beispiel-Session](#beispiel-session)
  * [Beispiel-Auswertung](#beispiel-auswertung)
  * [Installation](#installation)
    + [Vorbereitung](#vorbereitung)
    + [tuetermincheck-Repo klonen und ausführen](#tuetermincheck-repo-klonen-und-ausf&uuml;hren)
  * [Konfiguration](#konfiguration)
  * [Codes für Verwaltungsvorgänge](#codes-f&uuml;r-verwaltungsvorg&auml;nge)


## Beispiel-Abfrage

```
$ python3 tuetermincheck.py

Schlafe 4 Sekunden...
Starte Browser-Engine...
Starte Abfrage...


Termin für Ummeldung innerhalb Tübingens (2 Personen)
Abfragedatum: 2020-11-15
Nächster freier Termin: 2021-01-04
Wartezeit: 50 Tage (mehr als 7 Wochen)
```

## Beispiel-Auswertung

![Aktueller Verlauf Wartezeiten](./examples/tuetermincheck.png)

[Datenbasis (exakter Zeitpunkt der Abfrage, Tag der Abfrage, Tag des nächstmöglichen freien Termins, Differenz in Tagen)](examples/tuetermincheck.csv)

*Auswertung über mehrere Tage in einem "real life"-Anwendungsfall: Umzug in eine neue Wohnung innerhalb Tübingens. Laut Website der Stadt Tübingen: "Sie müssen sich innerhalb zwei Wochen nach Bezug der Wohnung bei der Meldebehörde anmelden. Wird diese Frist nicht eingehalten, droht eine Geldbuße."*

*Messwerte werden einmal täglich, direkt nach dem ersten Einschalten des PCs, erfasst - dieses Vorgehen orientiert sich an dem "Tipp" der Stadtverwaltung, man solle doch angesichts der teils erheblichen Wartezeiten auf einen Termin einfach täglich nachschauen, ob nicht doch wieder spontan ein früherer Termin freigeschaltet wurde.*

*Von einer - technisch problemlos möglichen - echten statistische Datenerhebung (z.B. 24/7 alle 15 Minuten ein Messwert für sämtliche online buchbaren Verwaltungsvorgänge, Erfassung der freien Zeitslots pro Tag etc.) wurde bewusst Abstand genommen, um das Online-Terminvereinbarungssystem der Stadt Tübingen nicht zu überlasten.*

## Installation

### Vorbereitung

Empfohlen wird die Installation in einer virtuellen Python-Umgebung:

```
$ python3 -mvenv tuetermincheck
$ cd tuetermincheck
$ . bin/activate
$ pip install --update pip
$ pip install wheel
```

- Mozillas Gecko-Treiber von https://github.com/mozilla/geckodriver/releases herunterladen, entpacken und in den Systempfad verschieben (z.B. nach /usr/local/bin )
- optional, empfohlen: Tor-Browser-Bundle von https://www.torproject.org/download/ herunterladen und entpacken.

### tuetermincheck-Repo klonen und ausführen
```
$ git clone https://codeberg.org/mats-ng/tuetermincheck.git
$ cd tuetermincheck
```
- benötigte Python-Packages installieren: `pip install -r requirements.txt`

- Script starten: `$ python3 tuetermincheck.py`

## Konfiguration

Folgende Parameter müssen direkt im Script angegeben werden:

- HEADLESS (*True*/False) - Die Abfrage kann wahlweise mit sichtbarem Browser oder "headless" durchgeführt werden.
- TIMEOUT (int) - Zeit in Sekunden, die auf das Laden einer Seite des Terminportals gewartet wird.
- ANONYYMOUS (*True*/False) - Als Browser stehen Firefox (nicht anonym, d.h. die eigene IP könnte geloggt werden) oder das Tor-Browser-Bundle (anonyme Abfrage; empfohlen!) zur Auswahl.
- TOR_BUNDLE_PATH (str) - Falls anonymer Zugriff gewünscht ist, muss hier der Pfad zum Hauptverzeichnis des Tor-Browser-Bundles angegeben werden
- SLEEP (int) - wenn > 0: Zeitspanne in Sekunden nach Start des Skripts, innerhalb derer die eigentliche Ausführung der Abfrage zufällig stattfinden soll (z.B. bei zeitgesteuertem Aufruf als cron-Job, damit die Abfrage _nicht_ belegbar täglich um exakt die selbe Zeit erfolgt). SLEEP=0 deaktiviert diese zufällige Verzögerung.
- DATA_DIR (path) - Verzeichnis, in dem die erfassten Daten gespeichert werden 
- LOGFILE (path) - Pfad zu einer CSV-Datei, an die die Messergebnisse angehängt werden, wenn für den jeweiligen Tag noch kein Messwert vorhanden ist
- VERWALTUNGSVORGANG (str) - Code für den jeweiligen Verwaltungsvorgang/Anliegen (siehe unten)
- ANZAHL_PERSONEN (int) - für die ein Termin beantragt werden soll

## Codes für Verwaltungsvorgänge

Für den Konfiguratiosparamter "VERWALTUNGSVORGANG" können folgende Werte gesetzt werden:

- *Anmeldung in Tübingen inkl. Adressänderung im Ausweis/Pass*: ANMELD-4620-mittermin
- *Anmeldung in Tübingen aus dem Ausland und der EU*: ANMAA-4779-mittermin
- *Ummeldung innerhalb Tübingen inkl. Adressänderung Ausweis/Pass*: UMMELD-4623-mittermin (default)
- *Abmeldung Nebenwohnsitz*: NWS-4617-mittermin
- *Abmeldung ins Ausland*: ABMELD-4616-mittermin
- *Nebenwohnung wird zur Hauptwohnung*: STATUSW-4628-mittermin
- *Lebensbescheinigung*: MELDLB-4624-mittermin
- *Einfache Meldebescheinigung*: MELDB-4625-mittermin
- *Erweiterte Meldebescheinigung*: MELDEE-4782-mittermin
- *Internationale Meldebescheinigung*: INTMB-4731-mittermin
- *Auskunftssperre im Melderegister beantragen*: MSPERR-4781-mittermin
- *Personalausweis beantragen*: BPA-4619-mittermin
- *Personalausweis beantragen mit Lichtbilderstellung*: BPALSST-4706-mittermin
- *Vorläufigen Personalausweis beantragen*: BPAVO-4630-mittermin
- *Vorläufiger Personalausweis mit Lichtbilderstellung*: VORLPASST-4784-mittermin
- *Adressänderung Personalausweis*: PBAAEND-4618-mittermin
- *PIN-Änderung/Freischaltung Online-Funktion Personalausweis*: PA-4765-mittermin
- *Reisepass oder Expressreisepass beantragen*: RP-4631-mittermin
- *Reisepass oder Expresspass mit Lichtbilderstellung*: BRPSST-4757-mittermin
- *Vorläufigen Reisepass beantragen*: RPVO-4632-mittermin
- *Vorläufiger Reisepass mit Lichtbilderstellung*: VORLRPSST-4785-mittermin
- *Wohnortänderung im Reisepass*: RPWOH-4764-mittermin
- *Kinderreisepass beantragen/verlängern*: KRP-4629-mittermin
- *Personalausweis/Reisepass Abholung*: BPARPABH-4633-mittermin
- *Verlustanzeige Personalausweis/Reisepass*: VERLP-4657-mittermin
- *Ausweisdokumente beantragen nach Einbürgerung mit Lichtbilderstellung*: EBPARPSST-4788-mittermin
- *Personalausweis beantragen nach Einbürgerung*: BPAEB-4915-mittermin
- *Reisepass oder Expressreisepass beantragen nach Einbürgerung*: BRPEB-4916-mittermin
- *Beglaubigung*: BEGL-4637-mittermin
- *Bewohnerparkausweis*: BPARK-4648-mittermin
- *Fischereischein Neuausstellung*: FISCHNEU-4621-mittermin
- *Fischereischein Verlängerung*: FISCHVERL-4638-mittermin
- *Führungszeugnis (einfach)*: EINFZ-4639-mittermin
- *Führungszeugnis (erweitert)*: ERWFZ-4640-mittermin
- *Führerscheinantrag*: FUEA-4634-mittermin
- *Fundsachen*: FUNDS-4644-mittermin
- *Gewerbezentralregisterauszug*: GEWZ-4641-mittermin
- *Landesfamilienpass*: LFP-4642-mittermin
- *Steuer-Identifikationnummer*: STID-4646-mittermin
