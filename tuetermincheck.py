#!/usr/bin/env python
#coding:utf-8
"""
  Author:  n.n.
  Purpose: Check free appointments of online appointment system of the City of Tuebingen
  Requirements: gecko driver (v0.23+) from https://github.com/mozilla/geckodriver/releases
      Optional: Tor-Browser bundle (for anonymous use)
  Additional Python packages: selenium, optional: tbselenium, stem
"""

from pathlib import Path
from datetime import date, datetime, timedelta
import csv

# Konfiguration
HEADLESS = False
TIMEOUT = 30
ANONYMOUS = True
TOR_BUNDLE_PATH = Path.home() / "Downloads/tor-browser_de"
SLEEP = 0

DATA_DIR = Path.home() / "tt"
LOGFILE = DATA_DIR / "tuetermincheck.csv"

VERWALTUNGSVORGANG = "UMMELD-4623-mittermin"
ANZAHL_PERSONEN = 2

STARTSEITE = "https://tempus-termine.com/termine/index.php?anlagennr=107"

# Konfiguration Ende

if SLEEP > 0:
    from time import sleep
    # erzeuge ganzzahlige Zufallszahl
    from random import randint
    t = randint(0, SLEEP)
    # warte eine zufällige Anzahl Sekunden
    print("Schlafe %s Sekunden..." % t)
    sleep(t)

print("Starte Browser-Engine...")
        
if ANONYMOUS == False:
    # Verwende normalen Firefox (warning: your IP may be logged)
    from selenium.webdriver import Firefox
    from selenium.webdriver.firefox.options import Options
else:
    # Verwende TOR browser bundle (anonymer Zugriff)
    import tbselenium.common as cm
    from tbselenium.tbdriver import TorBrowserDriver
    from tbselenium.utils import launch_tbb_tor_with_stem

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

if ANONYMOUS == False:
    opts = Options()
    opts.headless = HEADLESS
    assert opts.headless
    browser = Firefox(options=opts)

else:
    tbb_dir = TOR_BUNDLE_PATH.as_posix()
    tor_process = launch_tbb_tor_with_stem(tbb_path=tbb_dir)
    browser = TorBrowserDriver(tbb_dir, tor_cfg=cm.USE_STEM, tbb_logfile_path="/dev/null", headless=HEADLESS)

# Rufe Startseite des Terminportals auf und wähle den gewünschten Verwaltungsvorgang und die Anzahl der Personen aus
print("Starte Abfrage...\n\n")
browser.get(STARTSEITE)
personen = browser.find_element_by_id(VERWALTUNGSVORGANG)
personen.send_keys(str(ANZAHL_PERSONEN))

# Klicke auf "Termine vereinbaren"
personen.submit()

# Warte bis die folgende Seite geladen wurde und der Bestätigungsbutton sichtbar ist
wait = WebDriverWait(browser, TIMEOUT)
confirm = wait.until(EC.element_to_be_clickable((By.ID, 'confirm')))

# Bestätige, dass die Hinweise zur Kenntnis genommen wurden
confirm.click()

# NEUE Zwischenseite mit Landkarte -> Klick auf "Bürgerbüro Stadtmitte"
buergerbuero = wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "Bürgerbüro Stadtmitte")))
buergerbuero.click()

# Timestamp für Abruf:
now = datetime.now().isoformat()

# Warte bis die folgende Seite geladen wurde und die Kalender mit den nächsten freien Terminen angezeigt werden
try:
    next_page_loaded = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'monatevent')))
    freie_termine_verfuegbar = True
except:
    # keine freien Termine verfügbar oder Seite funktioniert nicht
    no_data_value = "None"
    freie_termine_verfuegbar = False

# Erstelle eine Liste aller Tage, die für diese Abfrage als freie Termine angezeigt werden (d.h. die einen anklickbaren "Event" haben)
freie_termine = []

alle_termine_mit_events = browser.find_elements_by_class_name("monatevent")

for event in alle_termine_mit_events:
    event_links = event.find_elements(By.TAG_NAME, "a")
    for link in event_links:
        href = link.get_property("href")
        datum = href.split("datum=")[1].split("&tasks")[0]
        freie_termine.append(datum)

# falls keine freien Termine verfügbar, schreibe no data Wert:
if not freie_termine_verfuegbar:
    freie_termine.append(no_data_value)

# Auswertung
print("Termin für Ummeldung innerhalb Tübingens (%s Personen)" % ANZAHL_PERSONEN)
print("Abfragedatum: %s" % date.today().isoformat())
print("Nächster freier Termin: %s" % freie_termine[0])

if freie_termine_verfuegbar:
    delta = date.fromisoformat(freie_termine[0]) - date.today()
    wartezeit_tage = delta.days
    print("Wartezeit: %s Tage (mehr als %s Wochen)" % (wartezeit_tage, int(wartezeit_tage/7)))
else:
    wartezeit_tage = no_data_value
    print("Wartezeit: unendlich")

# Auswertung fortlaufend in eine CSV-Datei schreiben, wenn dort noch keine Messung für diesen Tag erfasst wurde
heute = date.today().isoformat()
tag_bereits_erfasst = False

if Path.exists(LOGFILE):
    with open(LOGFILE, "r") as log_file:
        log_reader = csv.reader(log_file)
        for row in log_reader:
            try:
                if row[1] == heute:
                    print("\n\nEintrag für %s existiert bereits." % heute)
                    tag_bereits_erfasst = True
            except:
                tag_bereits_erfasst = False
            
if not tag_bereits_erfasst:
    with open(LOGFILE, "a+") as log_file:
        log_writer = csv.writer(log_file,  delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        log_writer.writerow([now, heute, freie_termine[0], wartezeit_tage])
        
        # Beweismittelsicherung (herauszoomen und einen Screenshot speichern, mit Timestamp des Abrufs im Dateinamen)
        browser.execute_script('document.body.style.MozTransform = "scale(0.6)";')
        bilddateiname = "%s.png" % now.replace("T", "_").replace(":", "-")
        bilddateipfad = DATA_DIR / bilddateiname
        browser.save_screenshot(bilddateipfad.as_posix())

# Beende den Selenium-Treiber / Browser
if ANONYMOUS == True:
    tor_process.kill()
browser.close()
